'''
Created on Aug 3, 2013

@author: Mauricio
'''

#read chosen method id , where 1= One foot method, 2 = Variable design point. if tank diameter is < 200 ft, suggesst method 1 else method 2

# read corrosion thickness , specific gravity , material, ceiling type and angle, etc. 

#read plate properties and dimensions. 

#calculate number of plates based on tank height and plate width

#calculate design stress. if tank diameter less than 200 ft use selected material id to  directly obtain stress from product design stress table
#else, get minimum yield stress from table, apply the proper correction factor and if its less than product design stress, use that. 

#Calculate hidrostatic stress directly from table

#1 foot method: calculate appropiate height for each ring and use appropiate stress to calculate thickness. height is from the bottom of ring to overflow level
#plus hidrostatic height 

#variable design point method: for first ring, compare result from 1foot method to a defined formula; choose the smallest. For design thickness choose largest of the 
#design value and hidrostatic value.

#for second ring calculate a ratio dependent on calculated thickness and plate height, depending on that value the second course thickness is calculated from a formula

