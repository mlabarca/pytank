'''
Created on Jul 25, 2013

@authors:  Julio, Mlabarca


'''

"""Import routines from common library""" 
from interface import I_file_input 
from T_dimensioning import T_dimensioning
from pint import UnitRegistry

def main():
    """Get dimensional data from interface, for now console"""
    T_conditions, ureg = I_file_input(1)
    """Dimension the tank using the list given by the interface """
    T_dimensions, T_dimensions_n = T_dimensioning(T_conditions,ureg)
    for i in range(len(T_dimensions)):
        print T_dimensions_n[i] + " = " + str(T_dimensions[i])
    
    """ Calculate levels based on dimensions  """
    #T_levels_calc(T_dimensions)
    """ Get design data from interface e.g. Design temperature, pressure...  """
    #T_design_conditions = I_design_console()
    """ Calculate thickness based on design data from  interface """ 
    #T_thickness = T_thickcalc(T_design_conditions)
    
    """ Rest of subroutines here"""
      
    
if __name__ == '__main__':
    main()