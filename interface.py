'''
Created on Jul 28, 2013

@author: Mauricio
'''
from ConfigParser import SafeConfigParser

from pint import UnitRegistry
ureg = UnitRegistry()
Q_ = ureg.Quantity
ureg.load_definitions('units.txt') 

def I_file_input(section):
    #reading input file
    parser = SafeConfigParser()
    parser.read('input.ini')
    
    #lists of units for input variables
    d_units = [ureg.cmph, ureg.h, 1, ureg.ft, ureg.ft, ureg.inch, ureg.inch, ureg.ft, ureg.ft]
    t_units = []
    
    #dictionary to interpret section choice
    section_dict = {1:("Dimensioning",d_units) , 2:("thickness",t_units)}
    
    #reading  the values for a section of choice and a list of names
    results= []
    names = []
    
    for name, result in parser.items(section_dict[section][0]):
        names.append(str(name))
        unit = parser.items(section_dict[section][0]).index((name,result))
        results.append(float(result)*d_units[unit])
    
    return results, ureg

#testing 
def test_file_input():
    dimensioning = I_file_input(1)
    d_units = [ureg.cmph.units, ureg.h.units, 1, ureg.ft.units, ureg.ft.units, ureg.inch.units, ureg.inch.units, ureg.ft.units, ureg.ft.units]
    t_units = []
    #print dimensioning[0].to(ureg.cfph)
    for d_input in dimensioning:
        if hasattr(d_input, 'units'):
            t_units.append(d_input.units)
        else:
            t_units.append(1)    
    assert t_units == d_units 
    return "Test passed"  

#print test_file_input()        
