""" Tank dimensioning subroutine """

#import common
from pint import UnitRegistry
# ureg = UnitRegistry()
# Q_ = ureg.Quantity
# ureg.load_definitions('units.txt') 
from math import pi as PI
 
def T_dimensioning(T_conditions, ureg):      
    """ Calculate: design flow & capacity, nominal capacity.  """

    design_flow_margin = 1.2
    design_flow = T_conditions[0] * design_flow_margin
    design_capacity = design_flow * T_conditions[1]
    nominal_capacity = design_capacity / T_conditions[2] 
    
    """ use proposed diameter , height  to calculate capacity""" 
    tank_capacity = PI*(T_conditions[3]**2)*T_conditions[4]/4
        
    """ Use flange size  , flange height, flange to LLL , HHL-top to calculate all heights required   """
    H_Bottom_Flge = T_conditions[6].to(ureg.ft)
    H_Flge_LLL = T_conditions[7]
    H_Bottom_LLL =  H_Flge_LLL + H_Bottom_Flge
    df= design_flow.to(ureg.cfm)
    Volume_in_5min = df * 5*ureg.min
    height_in_5min = Volume_in_5min / (PI*(T_conditions[3]**2)/4)
    if height_in_5min < 6*ureg.inch:
        H_LL_LLL = H_HL_HHL = 0.5*ureg.ft      
    else:
        H_LL_LLL = H_HL_HHL = height_in_5min
    
    H_HL_LL = T_conditions[4] - H_Bottom_LLL - H_LL_LLL - H_HL_HHL - T_conditions[8]
               
    """ Calculate normal and maximum operational capacity , operational margin """
    normal_ope_capacity = H_HL_LL * PI*(T_conditions[3]**2)/4
    height_max = T_conditions[4] - T_conditions[7] -  T_conditions[8]
    max_ope_capacity =height_max * PI*(T_conditions[3]**2)/4
    ope_margin = normal_ope_capacity.to(ureg.bbl) - design_capacity.to(ureg.bbl)
   
    var_results = [design_flow, design_capacity, nominal_capacity, tank_capacity, H_Bottom_Flge, 
    H_Bottom_LLL, H_LL_LLL, H_HL_HHL, H_HL_LL, normal_ope_capacity, max_ope_capacity, ope_margin]
    
    var_strings = ["design_flow", "design_capacity", "nominal_capacity", "tank_capacity", "H_Bottom_Flge", 
    "H_Bottom_LLL", "H_LL_LLL", "H_HL_HHL", "H_HL_LL", "normal_ope_capacity", "max_ope_capacity", "ope_margin"]
    
    return var_results, var_strings   
       
    
     
    
